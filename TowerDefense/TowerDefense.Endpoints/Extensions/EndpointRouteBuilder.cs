using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using TowerDefense.Transport;
using TowerDefense.Transport.WebSockets;

namespace TowerDefense.Endpoints.Extensions
{
    public static class EndpointRouteBuilder
    {
        public static IEndpointConventionBuilder MapGameWebSocket(this IEndpointRouteBuilder builder, string path)
        {
            return builder.Map(path, async context =>
            {
                var webSocketClientConnectionFactory = context.RequestServices.GetRequiredService<WebSocketClientConnectionFactory>();
                var clientConnectionManager = context.RequestServices.GetRequiredService<ClientConnectionManager>();

                if (context.WebSockets.IsWebSocketRequest)
                {
                    using var webSocket = await context.WebSockets.AcceptWebSocketAsync();

                    var webSocketCloseSource = new TaskCompletionSource();
                    var listeningTokenSource = new CancellationTokenSource();

                    var connection = webSocketClientConnectionFactory.Create(webSocket, WebSocketConnectionDataType.Text, webSocketCloseSource);

                    clientConnectionManager.Add(connection);

                    var listeningTask = connection.StartListeningAsync(listeningTokenSource.Token);

                    await Task.WhenAny(listeningTask, webSocketCloseSource.Task);

                    listeningTokenSource.Cancel();
                    await connection.CloseAsync();

                    clientConnectionManager.Remove(connection);
                    webSocketClientConnectionFactory.ReleaseId(connection.Id);
                }
                else
                {
                    context.Response.StatusCode = StatusCodes.Status400BadRequest;
                }
            });
        }
    }
}
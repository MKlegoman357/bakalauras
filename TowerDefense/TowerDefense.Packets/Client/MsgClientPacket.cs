namespace TowerDefense.Packets.Client
{
    [ClientPacket(ClientPacketType.MSG)]
    public class MsgClientPacket : ClientPacketBase
    {
        [Prop] public string Message { get; init; } = "";

        public MsgClientPacket() : base(ClientPacketType.MSG)
        {
        }
    }
}
using System;
using System.Linq;

namespace TowerDefense.Packets
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ClientPacketAttribute : Attribute
    {
        public int PacketTypeRaw { get; }

        public ClientPacketType PacketType { get; } = ClientPacketType.Custom;

        public ClientPacketAttribute(ClientPacketType packetType)
        {
            PacketType = packetType;
            PacketTypeRaw = (int) PacketType;
        }

        public ClientPacketAttribute(int packetTypeRaw)
        {
            PacketTypeRaw = packetTypeRaw;

            if (Enum.GetValues<ClientPacketType>().Any(t => (int) t == PacketTypeRaw))
            {
                PacketType = (ClientPacketType) PacketTypeRaw;
            }
        }
    }
}
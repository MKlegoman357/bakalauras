namespace TowerDefense.Packets
{
    public abstract class ClientPacketBase : IClientPacket
    {
        [Prop] public ClientPacketType Type { get; }

        protected ClientPacketBase(ClientPacketType type)
        {
            Type = type;
        }
    }
}
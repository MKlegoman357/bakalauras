using System.Reflection;

namespace TowerDefense.Packets
{
    public static class GlobalPacketRegistry
    {
        public static PacketRegistry ClientPacketRegistry { get; } = new();
        public static PacketRegistry ServerPacketRegistry { get; } = new();

        public static void AutoRegister()
        {
            AutoRegister(typeof(GlobalPacketRegistry).Assembly);
        }

        public static void AutoRegister(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                var clientPacketAttribute = type.GetCustomAttribute<ClientPacketAttribute>();
                var serverPacketAttribute = type.GetCustomAttribute<ServerPacketAttribute>();

                if (clientPacketAttribute != null)
                {
                    ClientPacketRegistry.Register(clientPacketAttribute.PacketTypeRaw, type);
                }

                if (serverPacketAttribute != null)
                {
                    ServerPacketRegistry.Register(serverPacketAttribute.PacketTypeRaw, type);
                }
            }
        }
    }
}
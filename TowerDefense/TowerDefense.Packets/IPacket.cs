using System;

namespace TowerDefense.Packets
{
    public interface IPacket
    {
    }

    public interface IPacket<out TType> : IPacket where TType : Enum
    {
        TType Type { get; }
    }
}
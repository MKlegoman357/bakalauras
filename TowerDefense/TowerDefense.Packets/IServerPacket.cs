namespace TowerDefense.Packets
{
    public interface IServerPacket : IPacket<ServerPacketType>
    {
    }
}
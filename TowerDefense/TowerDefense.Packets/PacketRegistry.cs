using System;
using System.Collections.Generic;
using System.Linq;

namespace TowerDefense.Packets
{
    public class PacketRegistry
    {
        private readonly Dictionary<int, Type> registeredPacketTypes = new();

        private readonly object registerLock = new();

        public void Register(int packetType, Type packet)
        {
            if (!packet.IsAssignableTo(typeof(IPacket)))
                throw new ArgumentException($"{nameof(packet)} must implement the {typeof(IPacket)} interface.");

            lock (registerLock)
            {
                if (registeredPacketTypes.ContainsValue(packet))
                    throw new ArgumentException($"Packet type {packet} has already been registered.");

                registeredPacketTypes[packetType] = packet;
            }
        }

        public Type? Get(int packetType)
        {
            // ReSharper disable once InconsistentlySynchronizedField
            return registeredPacketTypes.TryGetValue(packetType, out var packet) ? packet : null;
        }

        public IPacket? Construct(int packetType)
        {
            // ReSharper disable once InconsistentlySynchronizedField
            var type = registeredPacketTypes.TryGetValue(packetType, out var t) ? t : null;

            if (type == null) return null;

            var constructor = type.GetConstructor(Array.Empty<Type>());

            if (constructor == null)
                throw new Exception($"Public no-arg constructor for packet type {type} was not found.");

            var packet = (IPacket) constructor.Invoke(Array.Empty<object?>());

            return packet;
        }

        public bool Unregister(int packetType)
        {
            lock (registerLock)
            {
                return registeredPacketTypes.Remove(packetType);
            }
        }

        public bool Unregister(Type packet)
        {
            lock (registerLock)
            {
                var packetType = registeredPacketTypes
                    .Where(r => r.Value == packet)
                    .Select(r => (int?) r.Key)
                    .FirstOrDefault();

                return packetType != null && registeredPacketTypes.Remove(packetType.Value);
            }
        }
    }
}
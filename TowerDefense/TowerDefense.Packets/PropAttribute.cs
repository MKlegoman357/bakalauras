using System;
using System.Runtime.CompilerServices;

namespace TowerDefense.Packets
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PropAttribute : Attribute
    {
        public string? Name { get; }

        public int Order { get; }

        public int LineNumber { get; }

        public PropAttribute(string? name = null, int order = 0, [CallerLineNumber] int lineNumber = 0)
        {
            Name = name;
            Order = order;
            LineNumber = lineNumber;
        }
    }
}
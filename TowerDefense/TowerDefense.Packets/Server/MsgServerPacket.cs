namespace TowerDefense.Packets.Server
{
    [ServerPacket(ServerPacketType.MSG)]
    public class MsgServerPacket : ServerPacketBase
    {
        [Prop] public string Message { get; set; } = "";

        public MsgServerPacket() : base(ServerPacketType.MSG)
        {
        }
    }
}
using System;
using System.Linq;

namespace TowerDefense.Packets
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ServerPacketAttribute : Attribute
    {
        public int PacketTypeRaw { get; }

        public ServerPacketType PacketType { get; } = ServerPacketType.Custom;

        public ServerPacketAttribute(ServerPacketType packetType)
        {
            PacketType = packetType;
            PacketTypeRaw = (int) PacketType;
        }

        public ServerPacketAttribute(int packetTypeRaw)
        {
            PacketTypeRaw = packetTypeRaw;

            if (Enum.GetValues<ServerPacketType>().Any(t => (int) t == PacketTypeRaw))
            {
                PacketType = (ServerPacketType) PacketTypeRaw;
            }
        }
    }
}
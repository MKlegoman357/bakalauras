namespace TowerDefense.Packets
{
    public abstract class ServerPacketBase : IServerPacket
    {
        [Prop] public ServerPacketType Type { get; }

        protected ServerPacketBase(ServerPacketType type)
        {
            Type = type;
        }
    }
}
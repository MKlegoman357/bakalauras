namespace TowerDefense.Packets
{
    public enum ServerPacketType
    {
        Unknown = 0,

        MSG = 0x00000001,

        Custom = 0x11111111
    }
}
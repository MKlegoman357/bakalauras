using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TowerDefense.Packets.Utils
{
    public static class PacketUtils
    {
        private static readonly IComparer<Type> PropertyDeclaringTypeComparer = Comparer<Type>.Create(
            (a, b) => a.IsSubclassOf(b) ? 1 : b.IsSubclassOf(a) ? -1 : 0
        );

        public static ICollection<(PropertyInfo info, PropAttribute attr)> GetSerializableProperties<TPacket>() where TPacket : IPacket
        {
            return GetSerializableProperties(typeof(TPacket));
        }

        public static ICollection<(PropertyInfo info, PropAttribute attr)> GetSerializableProperties(Type packetType)
        {
            if (!packetType.IsAssignableTo(typeof(IPacket)))
                throw new ArgumentException($"{nameof(packetType)} does not implement the {typeof(IPacket)} interface.");

            var properties =
                from propertyInfo in packetType.GetProperties()
                let propAttribute = propertyInfo.GetCustomAttribute<PropAttribute>()
                let declaringType = propertyInfo.DeclaringType
                where propAttribute != null && declaringType != null
                select (propertyInfo, propAttribute, declaringType);

            return properties
                .OrderBy(p => p.propAttribute!.Order)
                .ThenBy(p => p.declaringType!, PropertyDeclaringTypeComparer)
                .ThenBy(p => p.propAttribute!.LineNumber)
                .Select(p => (p.propertyInfo!, p.propAttribute!))
                .ToList();
        }

        public static Type GetPacketTypeType<TPacket>() where TPacket : IPacket
        {
            return GetPacketTypeType(typeof(TPacket));
        }

        public static Type GetPacketTypeType(Type packetType)
        {
            if (!packetType.IsAssignableTo(typeof(IPacket)))
                throw new ArgumentException($"{nameof(packetType)} does not implement the {typeof(IPacket)} interface.");

            var genericPacketType = packetType.GetInterfaces().FirstOrDefault(i => i.IsConstructedGenericType && i.GetGenericTypeDefinition() == typeof(IPacket<>));

            if (genericPacketType == null)
                throw new ArgumentException($"{nameof(packetType)} does not implement the {typeof(IPacket<>)} interface.");

            return genericPacketType.GenericTypeArguments[0];
        }
    }
}
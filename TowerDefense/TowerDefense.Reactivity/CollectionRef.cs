using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TowerDefense.Reactivity
{
    public class CollectionRef<T> : RefBase, ICollectionRef<T>
    {
        protected readonly ICollection<T> collection;

        protected readonly object @lock = new();

        public int Count
        {
            get
            {
                lock (@lock)
                {
                    UpdatableTracker.TrackAccess(this);

                    return collection.Count;
                }
            }
        }

        public bool IsReadOnly
        {
            get
            {
                lock (@lock)
                {
                    UpdatableTracker.TrackAccess(this);

                    return collection.IsReadOnly;
                }
            }
        }

        public CollectionRef(ICollection<T> collection)
        {
            this.collection = collection;
        }

        public void Add(T item)
        {
            lock (@lock)
            {
                collection.Add(item);

                NotifyListeners();
            }
        }

        public void Clear()
        {
            lock (@lock)
            {
                collection.Clear();

                NotifyListeners();
            }
        }

        public bool Contains(T item)
        {
            lock (@lock)
            {
                UpdatableTracker.TrackAccess(this);

                return collection.Contains(item);
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            lock (@lock)
            {
                UpdatableTracker.TrackAccess(this);

                collection.CopyTo(array, arrayIndex);
            }
        }

        public bool Remove(T item)
        {
            lock (@lock)
            {
                var result = collection.Remove(item);

                NotifyListeners();

                return result;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            lock (@lock)
            {
                UpdatableTracker.TrackAccess(this);

                return collection.ToList().GetEnumerator();
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            lock (@lock)
            {
                UpdatableTracker.TrackAccess(this);

                return collection.ToList().GetEnumerator();
            }
        }
    }
}
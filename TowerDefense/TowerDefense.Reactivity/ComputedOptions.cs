namespace TowerDefense.Reactivity
{
    public class ComputedOptions
    {
        public bool Late { get; set; } = true;
    }
}
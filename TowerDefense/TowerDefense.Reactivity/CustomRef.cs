using System;

namespace TowerDefense.Reactivity
{
    public class CustomRef<T> : RefBase, IRef<T>
    {
        private readonly Func<T> getter;
        private readonly Action<T, Action> setter;

        public T Value
        {
            get
            {
                UpdatableTracker.TrackAccess(this);

                return getter();
            }

            set => setter(value, NotifyListeners);
        }

        public CustomRef(Func<T> getter, Action<T, Action> setter, Action<Action>? registerOnChange = null)
        {
            this.getter = getter;
            this.setter = setter;

            registerOnChange?.Invoke(NotifyListeners);
        }
    }
}
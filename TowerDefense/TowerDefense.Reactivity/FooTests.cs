using System;
using NUnit.Framework;

namespace TowerDefense.Reactivity
{
    public class Foo
    {
        public IRef<Bar> Bar { get; }

        public IRef<string> Text { get; }

        public Foo(Bar bar, string text)
        {
            Bar = new Ref<Bar>(bar);
            Text = new Ref<string>(text);
        }
    }

    public class Bar
    {
        public IRef<string> Text { get; }

        public Bar(string text)
        {
            Text = new Ref<string>(text);
        }
    }

    public class Testing
    {
        [Test]
        public void ComputedTest1()
        {
            var bar = new Bar("bar");
            var foo = new Foo(bar, "foo");

            var result1 = ReUtils.Computed(() => foo.Bar.Value.Text.Value);

            var subscription = result1.ToObservable().Subscribe(Console.WriteLine);

            bar.Text.Value += "1";

            Console.WriteLine($"s {result1.Value}");

            var bar2 = new Bar("bar2");

            foo.Bar.Value = bar2;

            Console.WriteLine($"s {result1.Value}");

            subscription.Dispose();

            foo.Bar.Value = bar;

            Console.WriteLine($"s {result1.Value}");
        }

        [Test]
        public void ComputedTest2()
        {
            var nameRef = new Ref<string>("foo");
            var lastNameRef = new Ref<string>("bar");

            nameRef.ToObservable(false).Subscribe(name => Console.WriteLine($"name: {name}"));
            lastNameRef.ToObservable(false).Subscribe(lastName => Console.WriteLine($"lastName: {lastName}"));

            var nameWithSuffixRef = ReUtils.Computed(() => nameRef.Value + "s");

            nameWithSuffixRef.ToObservable(false).Subscribe(nameWithSuffix => Console.WriteLine($"nameWithSuffix: {nameWithSuffix}"));

            var fullNameRef = ReUtils.Computed(() => nameRef.Value + " (" + nameWithSuffixRef.Value + ") " + lastNameRef.Value, fullName =>
            {
                var spaceIndex = fullName.IndexOf(' ');

                if (spaceIndex < 0)
                {
                    nameRef.Value = fullName;
                }
                else
                {
                    nameRef.Value = fullName.Substring(0, spaceIndex);
                    lastNameRef.Value = fullName.Substring(spaceIndex + 1);
                }
            }, new ComputedOptions {Late = false});

            fullNameRef.ToObservable().Subscribe(fullName => Console.WriteLine($"fullName: {fullName}"));

            nameRef.Value = "foos";
            lastNameRef.Value = "bars";

            fullNameRef.Value = "bar foo";
        }

        [Test]
        public void WatchTest1()
        {
            var bar1 = new Bar("bar");
            var bar2 = new Bar("bar2");

            var bar3 = new Bar("");

            bar3.Text.ToObservable().Subscribe(Console.WriteLine);

            var watch = ReUtils.Watch(new[] {bar1.Text, bar2.Text}, () =>
                {
                    bar3.Text.Value = bar1.Text.Value + "_" + bar2.Text.Value;
                },
                new WatchOptions {Immediate = true});

            bar1.Text.Value += "1";

            bar2.Text.Value += "2";

            watch.Dispose();

            bar1.Text.Value += "1";
        }

        [Test]
        public void WatchEffectTest1()
        {
            var bar1 = new Bar("bar");
            var bar2 = new Bar("bar2");

            var bar3 = new Bar("");

            bar3.Text.ToObservable().Subscribe(Console.WriteLine);

            var watch = ReUtils.WatchEffect(() =>
            {
                bar3.Text.Value = bar1.Text.Value + "_" + bar2.Text.Value;
            }, new WatchEffectOptions
            {
                IgnoredUpdatables = new[] {bar3.Text}
            });

            bar1.Text.Value += "1";

            bar2.Text.Value += "2";

            bar3.Text.Value = "f";

            watch.Dispose();

            bar1.Text.Value += "1";
        }
    }
}
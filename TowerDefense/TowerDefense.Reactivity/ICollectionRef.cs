using System.Collections.Generic;

namespace TowerDefense.Reactivity
{
    // ReSharper disable once PossibleInterfaceMemberAmbiguity
    public interface ICollectionRef<T> : ICollection<T>, IReadonlyCollectionRef<T>
    {
    }
}
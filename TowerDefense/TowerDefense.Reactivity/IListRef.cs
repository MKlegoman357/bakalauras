using System.Collections.Generic;

namespace TowerDefense.Reactivity
{
    // ReSharper disable once PossibleInterfaceMemberAmbiguity
    public interface IListRef<T> : IList<T>, IReadonlyListRef<T>
    {
    }
}
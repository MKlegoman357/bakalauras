using System.Collections.Generic;

namespace TowerDefense.Reactivity
{
    public interface IReadonlyCollectionRef<out T> : IReadOnlyCollection<T>, IUpdatable
    {
    }
}
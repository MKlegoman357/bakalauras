using System.Collections.Generic;

namespace TowerDefense.Reactivity
{
    public interface IReadonlyListRef<out T> : IReadOnlyList<T>, IUpdatable
    {
    }
}
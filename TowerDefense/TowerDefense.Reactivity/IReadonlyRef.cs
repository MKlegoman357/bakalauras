namespace TowerDefense.Reactivity
{
    public interface IReadonlyRef<out T> : IUpdatable
    {
        T Value { get; }
    }
}
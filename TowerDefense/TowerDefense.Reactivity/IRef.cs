namespace TowerDefense.Reactivity
{
    public interface IRef<T> : IReadonlyRef<T>
    {
        new T Value { get; set; }
    }
}
using System;

namespace TowerDefense.Reactivity
{
    public interface IUpdatable
    {
        IDisposable AddOnChangeListener(Action listener);
        IDisposable AddOnNextChangeListener(Action listener);
    }
}
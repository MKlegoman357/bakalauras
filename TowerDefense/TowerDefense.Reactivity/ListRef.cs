using System.Collections.Generic;

namespace TowerDefense.Reactivity
{
    public class ListRef<T> : CollectionRef<T>, IListRef<T>
    {
        public T this[int index]
        {
            get
            {
                lock (@lock)
                {
                    UpdatableTracker.TrackAccess(this);

                    return ((IList<T>) collection)[index];
                }
            }
            set
            {
                lock (@lock)
                {
                    ((IList<T>) collection)[index] = value;

                    NotifyListeners();
                }
            }
        }

        public ListRef(IList<T> collection) : base(collection)
        {
        }

        public int IndexOf(T item)
        {
            lock (@lock)
            {
                UpdatableTracker.TrackAccess(this);

                return ((IList<T>) collection).IndexOf(item);
            }
        }

        public void Insert(int index, T item)
        {
            lock (@lock)
            {
                ((IList<T>) collection).Insert(index, item);

                NotifyListeners();
            }
        }

        public void RemoveAt(int index)
        {
            lock (@lock)
            {
                ((IList<T>) collection).RemoveAt(index);

                NotifyListeners();
            }
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace TowerDefense.Reactivity
{
    public static class ReUtils
    {
        public static IObservable<T> ToObservable<T>(this IReadonlyRef<T> readonlyRef, bool immediate = true)
        {
            return Observable.Create<T>(observer =>
            {
                var changeListener = readonlyRef.AddOnChangeListener(() =>
                {
                    observer.OnNext(readonlyRef.Value);
                });

                if (immediate)
                {
                    observer.OnNext(readonlyRef.Value);
                }

                return () => changeListener.Dispose();
            });
        }

        public static IReadonlyRef<T> Computed<T>(Func<T> getter, ComputedOptions? options = null)
        {
            return Computed<T>(_ => getter(), options);
        }

        public static IReadonlyRef<T> Computed<T>(Func<T?, T> getter, ComputedOptions? options = null)
        {
            options ??= new ComputedOptions();

            var computedLock = new object();

            T? result = default;
            var isDirty = true;
            Action? notifyChange = null;

            ICollection<IDisposable> currentUpdatableListeners = new List<IDisposable>();

            T CallGetter()
            {
                lock (computedLock)
                {
                    result = Track(() => getter(result), currentUpdatableListeners, OnChange);

                    isDirty = false;

                    return result;
                }
            }

            void NotifyChanges()
            {
                notifyChange!();
            }

            void OnChange()
            {
                isDirty = true;

                if (options.Late)
                {
                    UpdatableTracker.OnNoTracking += NotifyChanges;
                }
                else
                {
                    NotifyChanges();
                }
            }

            return new ReadonlyRef<T>(() => isDirty ? CallGetter() : result!, notifyValueChange => notifyChange = notifyValueChange);
        }

        public static IRef<T> Computed<T>(Func<T?, T> getter, Action<T> setter, ComputedOptions? options = null)
        {
            return Computed(getter, (newValue, _) => setter(newValue), options);
        }

        public static IRef<T> Computed<T>(Func<T> getter, Action<T> setter, ComputedOptions? options = null)
        {
            return Computed<T>(_ => getter(), (newValue, _) => setter(newValue), options);
        }

        public static IRef<T> Computed<T>(Func<T> getter, Action<T, Action> setter, ComputedOptions? options = null)
        {
            return Computed(_ => getter(), setter, options);
        }

        public static IRef<T> Computed<T>(Func<T?, T> getter, Action<T, Action> setter, ComputedOptions? options = null)
        {
            options ??= new ComputedOptions();

            var computedLock = new object();

            T? result = default;
            var isDirty = true;
            Action? notifyChange = null;

            ICollection<IDisposable> currentUpdatableListeners = new List<IDisposable>();

            T CallGetter()
            {
                lock (computedLock)
                {
                    result = Track(() => getter(result), currentUpdatableListeners, OnChange);

                    isDirty = false;

                    return result;
                }
            }

            void NotifyChanges()
            {
                notifyChange!();
            }

            void OnChange()
            {
                isDirty = true;

                if (options.Late)
                {
                    UpdatableTracker.OnNoTracking += NotifyChanges;
                }
                else
                {
                    NotifyChanges();
                }
            }

            void CallSetter(T newValue, Action notifyChanges)
            {
                lock (computedLock)
                {
                    UpdatableTracker.StartTracking();

                    setter(newValue, notifyChanges);

                    UpdatableTracker.StopTracking();
                }
            }

            return new CustomRef<T>(
                () => isDirty ? CallGetter() : result!,
                CallSetter,
                notifyValueChange => notifyChange = notifyValueChange
            );
        }

        public static IDisposable Watch(Func<IEnumerable<IUpdatable>> targets, Func<IDisposable?> callback, WatchOptions? options = null)
        {
            return Watch((Func<object>) targets, callback, options);
        }

        public static IDisposable Watch(Func<IEnumerable<IUpdatable>> targets, Func<Action?> callback, WatchOptions? options = null)
        {
            return Watch((Func<object>) targets, () =>
            {
                var result = callback();

                return result == null ? null : Disposable.Create(result);
            }, options);
        }

        public static IDisposable Watch(Func<IEnumerable<IUpdatable>> targets, Action callback, WatchOptions? options = null)
        {
            return Watch((Func<object>) targets, () =>
            {
                callback();

                return null;
            }, options);
        }

        public static IDisposable Watch(IEnumerable<IUpdatable> targets, Func<IDisposable?> callback, WatchOptions? options = null)
        {
            return Watch(() => targets, callback, options);
        }

        public static IDisposable Watch(IEnumerable<IUpdatable> targets, Func<Action?> callback, WatchOptions? options = null)
        {
            return Watch(() => targets, () =>
            {
                var result = callback();

                return result == null ? null : Disposable.Create(result);
            }, options);
        }

        public static IDisposable Watch(IEnumerable<IUpdatable> targets, Action callback, WatchOptions? options = null)
        {
            return Watch(() => targets, callback, options);
        }

        public static IDisposable Watch(Func<IUpdatable> target, Func<IDisposable?> callback, WatchOptions? options = null)
        {
            return Watch((Func<object>) target, callback, options);
        }

        public static IDisposable Watch(Func<IUpdatable> target, Func<Action?> callback, WatchOptions? options = null)
        {
            return Watch((Func<object>) target, () =>
            {
                var result = callback();

                return result == null ? null : Disposable.Create(result);
            }, options);
        }

        public static IDisposable Watch(Func<IUpdatable> target, Action callback, WatchOptions? options = null)
        {
            return Watch((Func<object>) target, () =>
            {
                callback();

                return null;
            }, options);
        }

        public static IDisposable Watch(IUpdatable target, Func<IDisposable?> callback, WatchOptions? options = null)
        {
            return Watch(() => target, callback, options);
        }

        public static IDisposable Watch(IUpdatable target, Func<Action?> callback, WatchOptions? options = null)
        {
            return Watch(() => target, () =>
            {
                var result = callback();

                return result == null ? null : Disposable.Create(result);
            }, options);
        }

        public static IDisposable Watch(IUpdatable target, Action callback, WatchOptions? options = null)
        {
            return Watch(() => target, callback, options);
        }

        private static IDisposable Watch(Func<object> targets, Func<IDisposable?> callback, WatchOptions? options = null)
        {
            options ??= new WatchOptions();

            var watchLock = new object();
            ICollection<IDisposable> currentUpdatableListeners = new List<IDisposable>();
            IDisposable? callbackDisposable = null;

            void CallCallback()
            {
                lock (watchLock)
                {
                    CallTarget();
                    callbackDisposable?.Dispose();
                    callbackDisposable = callback();
                }
            }

            void OnChange()
            {
                if (options!.Late)
                {
                    UpdatableTracker.OnNoTracking += CallCallback;
                }
                else
                {
                    CallCallback();
                }
            }

            void CallTarget()
            {
                lock (watchLock)
                {
                    var result = Track(targets, currentUpdatableListeners, OnChange);

                    switch (result)
                    {
                        case IUpdatable updatable:
                            currentUpdatableListeners.Add(updatable.AddOnNextChangeListener(CallCallback));
                            break;
                        case IEnumerable resultEnumerable:
                            foreach (var resultItem in resultEnumerable)
                            {
                                if (resultItem is IUpdatable updatable)
                                {
                                    currentUpdatableListeners.Add(updatable.AddOnNextChangeListener(CallCallback));
                                }
                            }

                            break;
                    }
                }
            }

            void Dispose()
            {
                lock (watchLock)
                {
                    foreach (var currentUpdatableListener in currentUpdatableListeners)
                    {
                        currentUpdatableListener.Dispose();
                    }

                    currentUpdatableListeners.Clear();
                    callbackDisposable?.Dispose();
                }
            }

            if (options.Immediate)
            {
                OnChange();
            }
            else
            {
                CallTarget();
            }

            return Disposable.Create(Dispose);
        }

        public static IDisposable WatchEffect(Action callback, WatchEffectOptions? options = null)
        {
            return WatchEffect(() =>
            {
                callback();

                return (IDisposable?) null;
            }, options);
        }

        public static IDisposable WatchEffect(Func<Action?> callback, WatchEffectOptions? options = null)
        {
            return WatchEffect(() =>
            {
                var result = callback();

                return result == null ? null : Disposable.Create(result);
            }, options);
        }

        public static IDisposable WatchEffect(Func<IDisposable?> callback, WatchEffectOptions? options = null)
        {
            options ??= new WatchEffectOptions();

            var watchEffectLock = new object();
            IDisposable? callbackDisposable = null;
            ICollection<IDisposable> currentUpdatableListeners = new List<IDisposable>();

            void CallCallback()
            {
                lock (watchEffectLock)
                {
                    callbackDisposable?.Dispose();
                    callbackDisposable = Track(callback, currentUpdatableListeners, OnChange, options.IgnoredUpdatables);
                }
            }

            void OnChange()
            {
                if (options!.Late)
                {
                    UpdatableTracker.OnNoTracking += CallCallback;
                }
                else
                {
                    CallCallback();
                }
            }

            void Dispose()
            {
                lock (watchEffectLock)
                {
                    foreach (var currentUpdatableListener in currentUpdatableListeners)
                    {
                        currentUpdatableListener.Dispose();
                    }

                    currentUpdatableListeners.Clear();
                    callbackDisposable?.Dispose();
                }
            }

            OnChange();

            return Disposable.Create(Dispose);
        }

        private static T Track<T>(
            Func<T> factory,
            ICollection<IDisposable> currentUpdatableListeners,
            Action onChange,
            IEnumerable<IUpdatable>? ignoredUpdatables = null
        )
        {
            var ignored = ignoredUpdatables == null ? new HashSet<IUpdatable>() : ignoredUpdatables.ToHashSet();

            foreach (var currentUpdatableListener in currentUpdatableListeners)
            {
                currentUpdatableListener.Dispose();
            }

            currentUpdatableListeners.Clear();

            UpdatableTracker.StartTracking();

            var result = factory();

            var updatables = UpdatableTracker.StopTracking();

            foreach (var updatable in updatables)
            {
                if (!ignored.Contains(updatable))
                {
                    currentUpdatableListeners.Add(updatable.AddOnNextChangeListener(() =>
                    {
                        foreach (var currentUpdatableListener in currentUpdatableListeners)
                        {
                            currentUpdatableListener.Dispose();
                        }

                        currentUpdatableListeners.Clear();

                        onChange();
                    }));
                }
            }

            return result;
        }
    }
}
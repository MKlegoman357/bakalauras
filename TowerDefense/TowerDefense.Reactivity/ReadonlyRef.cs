using System;

namespace TowerDefense.Reactivity
{
    public class ReadonlyRef<T> : RefBase, IReadonlyRef<T>
    {
        private readonly Func<T> getter;

        public T Value
        {
            get
            {
                UpdatableTracker.TrackAccess(this);

                return getter();
            }
        }

        public ReadonlyRef(Func<T> getter, Action<Action> registerOnChange)
        {
            this.getter = getter;

            registerOnChange(NotifyListeners);
        }
    }
}
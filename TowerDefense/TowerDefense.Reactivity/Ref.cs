namespace TowerDefense.Reactivity
{
    public class Ref<T> : RefBase, IRef<T>
    {
        private T value;

        public T Value
        {
            get
            {
                UpdatableTracker.TrackAccess(this);

                return value;
            }

            set
            {
                this.value = value;

                NotifyListeners();
            }
        }

        public Ref(T value)
        {
            this.value = value;
        }
    }
}
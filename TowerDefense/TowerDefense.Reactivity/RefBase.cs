using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;

namespace TowerDefense.Reactivity
{
    public abstract class RefBase : IUpdatable, IDisposable
    {
        private readonly ICollection<Action> changeListeners = new HashSet<Action>();
        private ICollection<Action> nextChangeListeners = new HashSet<Action>();

        private readonly object listenerLock = new();

        public IDisposable AddOnChangeListener(Action listener)
        {
            lock (listenerLock)
            {
                changeListeners.Add(listener);

                return Disposable.Create(() =>
                {
                    lock (listenerLock)
                    {
                        changeListeners.Remove(listener);
                    }
                });
            }
        }

        public IDisposable AddOnNextChangeListener(Action listener)
        {
            lock (listenerLock)
            {
                var currentListeners = nextChangeListeners;

                currentListeners.Add(listener);

                return Disposable.Create(() =>
                {
                    lock (listenerLock)
                    {
                        currentListeners.Remove(listener);
                    }
                });
            }
        }

        protected void NotifyListeners()
        {
            lock (listenerLock)
            {
                var currentNextChangeListeners = nextChangeListeners.ToList();
                var oldNextChangeListeners = nextChangeListeners;

                nextChangeListeners = new HashSet<Action>();

                foreach (var listener in currentNextChangeListeners)
                {
                    if (oldNextChangeListeners.Contains(listener))
                    {
                        listener();
                    }
                }

                var currentChangeListeners = changeListeners.ToList();

                foreach (var listener in currentChangeListeners)
                {
                    if (changeListeners.Contains(listener))
                    {
                        listener();
                    }
                }
            }
        }

        public void Dispose()
        {
            lock (listenerLock)
            {
                nextChangeListeners.Clear();
                changeListeners.Clear();
            }
        }
    }
}
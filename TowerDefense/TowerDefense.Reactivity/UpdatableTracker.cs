using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace TowerDefense.Reactivity
{
    public static class UpdatableTracker
    {
        private static readonly AsyncLocal<LinkedList<CurrentExecutionFlowInfo>?> CurrentFlowInfo = new();

        public static event Action OnNoTracking
        {
            add
            {
                var currentFlowInfo = CurrentFlowInfo.Value?.First?.Value;

                if (currentFlowInfo == null)
                {
                    value();
                }
                else
                {
                    currentFlowInfo.OnNoTracking += value;
                }
            }

            remove
            {
                var currentFlowInfo = CurrentFlowInfo.Value?.First?.Value;

                if (currentFlowInfo != null)
                {
                    currentFlowInfo.OnNoTracking -= value;
                }
            }
        }

        public static void TrackAccess(IUpdatable updatable)
        {
            var currentFlowInfo = CurrentFlowInfo.Value?.Last?.Value;

            if (currentFlowInfo != null)
            {
                currentFlowInfo.Updatables = currentFlowInfo.Updatables.Append(updatable);
            }
        }

        public static void StartTracking()
        {
            CurrentFlowInfo.Value ??= new LinkedList<CurrentExecutionFlowInfo>();

            CurrentFlowInfo.Value.AddLast(new CurrentExecutionFlowInfo());
        }

        public static ISet<IUpdatable> StopTracking()
        {
            var currentFlowInfoStack = CurrentFlowInfo.Value;

            if (currentFlowInfoStack == null || currentFlowInfoStack.Count <= 0) throw new Exception("No updatables are being tracked.");

            var currentFlowInfo = currentFlowInfoStack.Last!.Value;

            currentFlowInfoStack.RemoveLast();

            if (currentFlowInfoStack.Count <= 0)
            {
                CurrentFlowInfo.Value = null;
                currentFlowInfo.InvokeOnNoTracking();
            }

            return currentFlowInfo.Updatables.ToHashSet();
        }

        private class CurrentExecutionFlowInfo
        {
            public IEnumerable<IUpdatable> Updatables { get; set; }

            public event Action? OnNoTracking;

            public CurrentExecutionFlowInfo()
            {
                Updatables = Array.Empty<IUpdatable>();
            }

            public void InvokeOnNoTracking()
            {
                OnNoTracking?.Invoke();
            }
        }
    }
}
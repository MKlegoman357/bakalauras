using System.Collections.Generic;

namespace TowerDefense.Reactivity
{
    public class WatchEffectOptions
    {
        public bool Late { get; set; } = true;

        public IEnumerable<IUpdatable>? IgnoredUpdatables { get; set; } = null;
    }
}
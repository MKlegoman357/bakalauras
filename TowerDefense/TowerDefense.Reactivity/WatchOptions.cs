namespace TowerDefense.Reactivity
{
    public class WatchOptions
    {
        public bool Immediate { get; set; } = false;

        public bool Late { get; set; } = true;
    }
}
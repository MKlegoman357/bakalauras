using TowerDefense.Packets;

namespace TowerDefense.Serialization.Packet
{
    public interface IPacketSerializer<TSerialized>
    {
        TSerialized Serialize(IPacket packet);

        IPacket? Deserialize(TSerialized serializedPacket);
    }
}
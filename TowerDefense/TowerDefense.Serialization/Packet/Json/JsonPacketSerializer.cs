using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TowerDefense.Packets;

namespace TowerDefense.Serialization.Packet.Json
{
    public class JsonPacketSerializer : IPacketSerializer<string>
    {
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public JsonPacketSerializer(PacketRegistry packetRegistry)
        {
            jsonSerializerSettings = new JsonSerializerSettings
            {
                Converters = new List<JsonConverter>
                {
                    new PacketJsonConverter(packetRegistry)
                },
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                }
            };
        }

        public string Serialize(IPacket packet)
        {
            return JsonConvert.SerializeObject(packet, jsonSerializerSettings);
        }

        public IPacket? Deserialize(string serializedPacket)
        {
            return JsonConvert.DeserializeObject(serializedPacket, typeof(IPacket), jsonSerializerSettings) as IPacket;
        }
    }
}
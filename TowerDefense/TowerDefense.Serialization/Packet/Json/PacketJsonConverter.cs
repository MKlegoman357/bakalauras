using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using TowerDefense.Packets;
using TowerDefense.Packets.Utils;

namespace TowerDefense.Serialization.Packet.Json
{
    public class PacketJsonConverter : JsonConverter
    {
        private readonly PacketRegistry packetRegistry;

        public PacketJsonConverter(PacketRegistry packetRegistry)
        {
            this.packetRegistry = packetRegistry;
        }

        public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();

                return;
            }

            var properties = PacketUtils.GetSerializableProperties(value.GetType());

            writer.WriteStartObject();

            foreach (var (info, attr) in properties)
            {
                if (!info.CanRead) continue;

                var propName = GetPropertyName(info, attr, serializer);

                writer.WritePropertyName(propName);
                serializer.Serialize(writer, info.GetValue(value));
            }

            writer.WriteEndObject();
        }

        public override object? ReadJson(JsonReader reader, Type objectType, object? o, JsonSerializer serializer)
        {
            var packetRaw = JToken.Load(reader);

            if (packetRaw.Type == JTokenType.Null) return null;

            if (packetRaw.Type != JTokenType.Object)
                throw new Exception($"Packet must be an object, {packetRaw.Type} found. {packetRaw}");

            var packetTypeToken = packetRaw["type"];

            if (packetTypeToken == null)
                throw new Exception($"Packet type is missing. {packetRaw}");

            if (packetTypeToken.Type != JTokenType.Integer)
                throw new Exception($"Packet type must be an integer, {packetTypeToken.Type} found. {packetRaw}");

            var packetType = packetTypeToken.Value<int>();
            var packet = packetRegistry.Construct(packetType);

            if (packet == null)
                throw new Exception($"Packet type {packetType} is not registered in the packet registry. {packetRaw}");

            var properties = PacketUtils.GetSerializableProperties(packet.GetType());

            foreach (var (info, attr) in properties)
            {
                if (!info.CanWrite) continue;

                var propName = GetPropertyName(info, attr, serializer);
                var propToken = packetRaw[propName];

                if (propToken == null)
                    throw new Exception($"Missing property \"{propName}\" for packet {packet.GetType()}. {packetRaw}");

                var propValue = propToken.ToObject(info.PropertyType, serializer);

                info.SetValue(packet, propValue);
            }

            return packet;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsAssignableTo(typeof(IPacket));
        }

        private string GetPropertyName(PropertyInfo info, PropAttribute attr, JsonSerializer serializer)
        {
            var propName = attr.Name ?? info.Name;

            if (serializer.ContractResolver is DefaultContractResolver contractResolver)
            {
                propName = contractResolver.GetResolvedPropertyName(propName);
            }

            return propName;
        }
    }
}
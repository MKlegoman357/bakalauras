using System;
using System.Collections.Generic;
using TowerDefense.Reactivity;

namespace TowerDefense.Transport
{
    public class ClientConnectionManager
    {
        private readonly object @lock = new();
        private readonly ICollectionRef<IClientConnection> connectionsRef;

        // ReSharper disable once InconsistentlySynchronizedField
        public IReadonlyCollectionRef<IClientConnection> ConnectionsRef => connectionsRef;

        public event Action<IClientConnection>? OnNewConnection;
        public event Action<IClientConnection>? OnConnectionClosed;

        public ClientConnectionManager()
        {
            connectionsRef = new CollectionRef<IClientConnection>(new HashSet<IClientConnection>());
        }

        public void Add(IClientConnection clientConnection)
        {
            if (!clientConnection.IsOpen) return;

            lock (@lock)
            {
                if (connectionsRef.Contains(clientConnection)) return;

                connectionsRef.Add(clientConnection);

                OnNewConnection?.Invoke(clientConnection);

                clientConnection.OnClose += () => Remove(clientConnection);
            }
        }

        public void Remove(IClientConnection clientConnection)
        {
            lock (@lock)
            {
                if (connectionsRef.Remove(clientConnection))
                {
                    OnConnectionClosed?.Invoke(clientConnection);
                }
            }
        }
    }
}
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TowerDefense.Packets;
using TowerDefense.Serialization.Packet.Json;
using TowerDefense.Transport.WebSockets;

namespace TowerDefense.Transport.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddGameTransportServices(this IServiceCollection services)
        {
            return services
                .AddSingleton(provider => new WebSocketClientConnectionFactory(
                    new JsonPacketSerializer(GlobalPacketRegistry.ClientPacketRegistry),
                    provider.GetRequiredService<ILoggerFactory>()
                ))
                .AddSingleton<ClientConnectionManager>();
        }
    }
}
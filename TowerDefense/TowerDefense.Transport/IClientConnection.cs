using System;
using System.Threading;
using System.Threading.Tasks;
using TowerDefense.Packets;

namespace TowerDefense.Transport
{
    public interface IClientConnection
    {
        string Id { get; }

        bool IsOpen { get; }

        event Action<IClientPacket> OnClientPacket;

        event Action OnClose;

        Task SendAsync(IServerPacket packet, CancellationToken cancellationToken = default);

        Task CloseAsync();
    }
}
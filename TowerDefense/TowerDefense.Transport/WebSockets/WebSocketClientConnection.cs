using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TowerDefense.Packets;
using TowerDefense.Serialization.Packet;

namespace TowerDefense.Transport.WebSockets
{
    public class WebSocketClientConnection : IClientConnection
    {
        public const int ReceiveBufferSize = 4 * 1024;

        private readonly IPacketSerializer<string> textPacketSerializer;
        private readonly ILogger logger;
        private readonly TaskCompletionSource webSocketCloseSource;

        private readonly object @lock = new();

        public string Id { get; }

        private bool isOpen = true;

        public bool IsOpen => WebSocket.State == WebSocketState.Open && isOpen;

        public bool IsListening { get; private set; }

        public WebSocketConnectionDataType DataType { get; }

        public event Action<IClientPacket>? OnClientPacket;

        // ReSharper disable once InconsistentNaming
        private event Action? onClose;

        public event Action OnClose
        {
            add
            {
                lock (@lock)
                {
                    if (IsOpen)
                    {
                        onClose += value;
                    }
                    else
                    {
                        onClose?.Invoke();
                    }
                }
            }
            remove => onClose -= value;
        }

        public WebSocket WebSocket { get; private set; }

        public WebSocketClientConnection(
            string id,
            WebSocketConnectionDataType dataType,
            WebSocket webSocket,
            IPacketSerializer<string> textPacketSerializer,
            ILogger logger,
            TaskCompletionSource webSocketCloseSource
        )
        {
            this.textPacketSerializer = textPacketSerializer;
            this.logger = logger;
            this.webSocketCloseSource = webSocketCloseSource;

            Id = id;
            DataType = dataType;
            WebSocket = webSocket;
        }

        public async Task StartListeningAsync(CancellationToken stoppingToken = default)
        {
            lock (@lock)
            {
                if (!IsOpen) throw new InvalidOperationException("WebSocket is closed.");
                if (IsListening) throw new InvalidOperationException("WebSocket is already listening for messages.");

                IsListening = true;
            }

            var webSocketCloseTokenSource = new CancellationTokenSource();
            var linkedCancellationSource = CancellationTokenSource.CreateLinkedTokenSource(webSocketCloseTokenSource.Token, stoppingToken);

#pragma warning disable 4014
            // ReSharper disable once MethodSupportsCancellation
            webSocketCloseSource.Task.ContinueWith(_ => webSocketCloseTokenSource.Cancel());
#pragma warning restore 4014

            var receiveBuffer = WebSocket.CreateServerBuffer(ReceiveBufferSize);

            byte[] resultBuffer = Array.Empty<byte>();
            var resultBufferSize = 0;

            while (!linkedCancellationSource.IsCancellationRequested)
            {
                try
                {
                    var receiveResult = await WebSocket.ReceiveAsync(receiveBuffer, linkedCancellationSource.Token);

                    switch (receiveResult.MessageType)
                    {
                        case WebSocketMessageType.Text:
                        case WebSocketMessageType.Binary:
                            EnsureResultBufferSize(receiveResult.Count);

                            for (var i = 0; i < receiveResult.Count; i++)
                            {
                                resultBuffer[i + resultBufferSize] = receiveBuffer[i];
                            }

                            resultBufferSize += receiveResult.Count;

                            if (receiveResult.EndOfMessage)
                            {
                                if (receiveResult.MessageType == WebSocketMessageType.Text)
                                {
                                    var textResult = Encoding.UTF8.GetString(resultBuffer, 0, resultBufferSize);

                                    if (textPacketSerializer.Deserialize(textResult) is IClientPacket packet)
                                    {
                                        OnClientPacket?.Invoke(packet);
                                    }
                                    else
                                    {
                                        logger.LogError("Failed to deserialize websocket packet: {TextResult}", textResult);
                                    }
                                }
                                else
                                {
                                    logger.LogError(
                                        "Received a binary websocket message of length: {ResultBufferSize}. Binary messages are not currently supported",
                                        resultBufferSize
                                    );
                                }

                                resultBuffer = Array.Empty<byte>();
                                resultBufferSize = 0;
                            }

                            break;
                        case WebSocketMessageType.Close:
                            return;
                    }
                }
                catch (TaskCanceledException)
                {
                    return;
                }
                catch (Exception e)
                {
                    resultBuffer = Array.Empty<byte>();
                    resultBufferSize = 0;

                    logger.LogError(e, "An error occured while listening to websocket messages");
                }
            }

            void EnsureResultBufferSize(int additionalBytes)
            {
                var neededBufferBytes = resultBufferSize + additionalBytes;

                if (resultBuffer.Length >= neededBufferBytes) return;

                var newBufferSize = (int) Math.Pow(2, (int) Math.Ceiling(Math.Log2(neededBufferBytes)));
                var newBuffer = new byte[newBufferSize];

                resultBuffer.CopyTo(newBuffer, 0);

                resultBuffer = newBuffer;
            }
        }

        public async Task SendAsync(IServerPacket packet, CancellationToken cancellationToken = default)
        {
            ArraySegment<byte> buffer;
            WebSocketMessageType messageType;

            lock (@lock)
            {
                if (!IsOpen) throw new InvalidOperationException("WebSocket connection is closed.");

                if (DataType == WebSocketConnectionDataType.Text)
                {
                    var serializedPacket = textPacketSerializer.Serialize(packet);
                    var textBytes = Encoding.UTF8.GetBytes(serializedPacket);

                    buffer = new ArraySegment<byte>(textBytes);
                    messageType = WebSocketMessageType.Text;
                }
                else
                {
                    throw new Exception($"Only {WebSocketConnectionDataType.Text} is supported for sending messages right now. Cannot send message as {DataType}.");
                }
            }

            await WebSocket.SendAsync(buffer, messageType, true, cancellationToken);
        }

        public async Task CloseAsync()
        {
            lock (@lock)
            {
                if (!IsOpen) return;

                onClose?.Invoke();
                isOpen = false;
            }

            await WebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, null, CancellationToken.None);
            webSocketCloseSource.TrySetResult();
        }
    }
}
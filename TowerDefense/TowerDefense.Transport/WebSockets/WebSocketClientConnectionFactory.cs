using System;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TowerDefense.Serialization.Packet;

namespace TowerDefense.Transport.WebSockets
{
    public class WebSocketClientConnectionFactory
    {
        private readonly ConcurrentDictionary<string, string> usedConnectionIds = new();

        private readonly IPacketSerializer<string> textPacketSerializer;
        private readonly ILoggerFactory loggerFactory;

        public WebSocketClientConnectionFactory(
            IPacketSerializer<string> textPacketSerializer,
            ILoggerFactory loggerFactory
        )
        {
            this.textPacketSerializer = textPacketSerializer;
            this.loggerFactory = loggerFactory;
        }

        public WebSocketClientConnection Create(
            WebSocket webSocket,
            WebSocketConnectionDataType dataType,
            TaskCompletionSource webSocketCloseSource
        )
        {
            var id = GenerateNewId();

            return new WebSocketClientConnection(
                id,
                dataType,
                webSocket,
                textPacketSerializer,
                loggerFactory.CreateLogger($"{typeof(WebSocketClientConnection).FullName}[id={id}]"),
                webSocketCloseSource
            );
        }

        public void ReleaseId(string id)
        {
            usedConnectionIds.TryRemove(id, out _);
        }

        private string GenerateNewId()
        {
            string id;
            var attemptsLeft = 5;

            do
            {
                if (attemptsLeft <= 0) throw new Exception("Failed to generate a unique connection id.");

                id = Guid.NewGuid().ToString();
                attemptsLeft--;
            } while (!usedConnectionIds.TryAdd(id, id));

            return id;
        }
    }
}
namespace TowerDefense.Transport.WebSockets
{
    public enum WebSocketConnectionDataType
    {
        Text,
        Binary
    }
}